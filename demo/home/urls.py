from django.urls import path
from django.contrib.auth import views as auth_views

from home import views

app_name = "home"


urlpatterns = [
    path('', views.Index.as_view(), name="index"),
    path('about/', views.About.as_view(), name="about"),
    path('login/', auth_views.LoginView.as_view(template_name="home/login.html"), name="login"),
    path('logout/', auth_views.LogoutView.as_view(template_name="home/login.html"), name="logout"),
    path('list/', views.List_Client.as_view(), name="list"),
    path('signup/', views.Signup.as_view(), name="signup"),
]
