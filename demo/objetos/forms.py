from django import forms

from .models import Objeto

class NewObjetoForm(forms.ModelForm):
    class Meta:
        model = Objeto
        fields = [
            "description",
            "obra",
            "artista",
        ]
        labels = {
            "description": "Nombre del objeto",
            "obra": "Obra",
            "artista": "Artista"
        }
        widget = {
            "description": forms.TextInput
        }

    def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)
            for field in iter(self.fields):
                self.fields[field].widget.attrs.update({
                    "class": "form-control"
                })
