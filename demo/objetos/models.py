from django.db import models

from pinturas.models import BaseModel,Obra,Artista

# Create your models here.

class Objeto(BaseModel):
    description = models.CharField(max_length=128, unique=True)
    obra = models.ForeignKey(Obra, on_delete=models.CASCADE)
    artista = models.ForeignKey(Artista, on_delete=models.CASCADE)



    def __str__(self):
        return "{}".format(self.description)


    def save(self):
        self.description = self.description.upper()
        super(Objeto, self).save()


    class  Meta:
        verbose_name_plural = "Objetos"
