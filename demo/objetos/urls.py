from django.urls import path

from objetos import views

app_name = "objetos"

urlpatterns =[
    path('list_objetos/', views.ListObjetos.as_view(), name="list_objetos"),
    path('new_objetos/', views.NewObjetos.as_view(), name="new_objetos"),
]
