from django.shortcuts import render
from django.views import generic
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy

from .models import Objeto
from .forms import NewObjetoForm
# Create your views here.

##### CRUDobjeto #####

## Create
## generic.CreateView
class NewObjeto(LoginRequiredMixin, generic.CreateView):
    template_name = "objetos/new_objeto.html"
    model = Objeto
    context_object_name = "obj"
    form_class = NewObjetoForm
    login_url = "home:login"
    success_url = reverse_lazy("objetos:listobjetos")

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super().form_valid(form)


## Retrieve
# generic.ListView
class ListObjeto(LoginRequiredMixin, generic.ListView):
    template_name = "objetos/list_objeto.html"
    queryset = Objeto.objects.all()
    login_url = "home:login"
    context_object_name = "obj"

## Update

class UpdateObjeto(LoginRequiredMixin, generic.UpdateView):
    template_name = "objetos/new_objeto.html"
    model = Objeto
    context_object_name = "obj"
    form_class = NewObjetoForm
    login_url = "home:login"
    success_url = reverse_lazy("objetos:list_objeto")

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super().form_valid(form)
## Delete
