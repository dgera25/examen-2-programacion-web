from django.apps import AppConfig


class PinturasConfig(AppConfig):
    name = 'pinturas'
