from django import forms

from .models import Category, Era, Museo, Artista, Obra

class NewCategoryForm(forms.ModelForm):
    class Meta:
        model = Category
        fields = [
            "description",
            "status",
        ]
        labels = {
            "description": "Category",
            "status": "Status"
        }
        widget = {
            "description": forms.TextInput
        }

    def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)
            for field in iter(self.fields):
                self.fields[field].widget.attrs.update({
                    "class": "form-control"
                })
class NewEraForm(forms.ModelForm):
    class Meta:
        model = Era
        fields = [
            "category",
            "description",
            "status",
        ]
        labels = {
            "description": "Category",
            "status": "Status"
        }
        widget = {
            "description": forms.TextInput
        }

    def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)
            for field in iter(self.fields):
                self.fields[field].widget.attrs.update({
                    "class": "form-control"
                })
            self.fields["category"].empty_label = "Choose Category"


class MuseoForm(forms.ModelForm):
    class Meta:
        model = Museo
        fields = [
            "description",
            "status",
        ]
        labels = {
            "description": "Museo",
            "status": "Status"
        }
        widget = {
            "description": forms.TextInput
        }

    def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)
            for field in iter(self.fields):
                self.fields[field].widget.attrs.update({
                    "class": "form-control"
                })

class ArtistaForm(forms.ModelForm):
    class Meta:
        model = Artista
        fields = [
            "description",
            "status",
        ]
        labels = {
            "description": "Artista",
            "status": "Status"
        }
        widget = {
            "description": forms.TextInput
        }

    def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)
            for field in iter(self.fields):
                self.fields[field].widget.attrs.update({
                    "class": "form-control"
                })




class ObraForm(forms.ModelForm):
    class Meta:
        model = Obra
        fields = [ ## "__all__"
            "code",
            "description",
            "status",
            "price",
            "last_buy",
            "museo",
            "era",
            "artista"
        ]
        exclude = ["user", "user_update", "timestamp", "update"]
        labels = {
            "code": "Code",
            "description": "Product Description",
            "status": "Status",
            "price": "Price",
            "last_buy": "Last Buy",
            "museo": "Museo",
            "era": "Era",
            "artista": "Artista",
        }
        widget = {
            "description": forms.TextInput
        }

    def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)
            for field in iter(self.fields):
                self.fields[field].widget.attrs.update({
                    "class": "form-control"
                })
