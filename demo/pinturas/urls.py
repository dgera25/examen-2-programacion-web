from django.urls import path

from pinturas import views

app_name = "pinturas"

urlpatterns = [
    path('list_category/', views.ListCategory.as_view(), name="list_category"),
    path('new_category/', views.NewCategory.as_view(), name="new_category"),
    path('update_category/<int:pk>/', views.UpdateCategory.as_view(), name="update_category"),
    path('delete_category/<int:pk>/', views.DeleteCategory.as_view(), name="delete_category"),
    path('new_era/', views.NewEra.as_view(), name="new_era"),
    path('list_era/', views.ListEra.as_view(), name="list_era"),
    path('update_era/<int:pk>/', views.UpdateEra.as_view(), name="update_era"),
    path('delete_era/<int:pk>/', views.DeleteEra.as_view(), name="delete_era"),
    path('detail_category/<int:pk>/', views.DetailCategory.as_view(), name="detail_category"),
    path('detail_era/<int:pk>/', views.DetailEra.as_view(), name="detail_era"),
    path('ws/list_category/', views.wsList, name="ws_list_category"),
     path('list_museo/', views.ListMuseo.as_view(), name="list_museo"),
    path('new_museo/', views.NewMuseo.as_view(), name="new_museo"),
    path('update_museo/<int:pk>/', views.UpdateMuseo.as_view(), name="update_museo"),
    path('delete_museo/<int:pk>/', views.DeleteMuseo.as_view(), name="delete_museo"),
     path('list_artista/', views.ListArtista.as_view(), name="list_artista"),
    path('new_artista/', views.NewArtista.as_view(), name="new_artista"),
    path('update_artista/<int:pk>/', views.UpdateArtista.as_view(), name="update_artista"),
    path('delete_artista/<int:pk>/', views.DeleteArtista.as_view(), name="delete_artista"),
    path('list_obra/', views.ListObra.as_view(), name="list_obra"),
    path('new_obra/', views.NewObra.as_view(), name="new_obra"),
    path('update_obra/<int:pk>/', views.UpdateObra.as_view(), name="update_obra"),
]
