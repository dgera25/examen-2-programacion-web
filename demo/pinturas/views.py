from django.shortcuts import render
from django.urls import reverse_lazy

from django.views import generic
from django.contrib.auth.mixins import LoginRequiredMixin

from django.http import JsonResponse, HttpResponse
from django.core import serializers

from .models import Category, Era, Museo, Artista, Obra
from .forms import NewCategoryForm, NewEraForm, MuseoForm, ArtistaForm, ObraForm
# Create your views here.


##### CRUD Category #####


##### RETRIEVE (List, Detail) #####
## generic.ListView
class ListCategory(LoginRequiredMixin, generic.ListView):
    template_name = "pinturas/list_category.html"
    model = Category
    context_object_name = "obj"
    login_url = "home:login"

## generic.DetailView
class DetailCategory(LoginRequiredMixin, generic.DetailView):
    template_name = "pinturas/detail_category.html"
    model = Category
    login_url = "home:login"
    context_object_name = "obj"

class NewCategory(LoginRequiredMixin, generic.CreateView):
    template_name = "pinturas/new_category.html"
    model = Category
    context_object_name = "obj"
    form_class = NewCategoryForm
    login_url = "home:login"
    success_url = reverse_lazy("pinturas:list_category")

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super().form_valid(form)

##### UPDATE #####
## generic.UpdateView
class UpdateCategory(LoginRequiredMixin, generic.UpdateView):
    template_name = "pinturas/update_category.html"
    model = Category
    context_object_name = "obj"
    form_class = NewCategoryForm
    login_url = "home:login"
    success_url = reverse_lazy("pinturas:list_category")

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super().form_valid(form)



##### DELETE #####
## generic.DeleteView
class DeleteCategory(LoginRequiredMixin, generic.DeleteView):
    template_name = "pinturas/delete_category.html"
    model = Category
    context_object_name = "obj"
    login_url = "home:login"
    success_url = reverse_lazy("pinturas:list_category")


##### CRUD ERA #####

##### RETRIEVE #####
## generic.ListView
class ListEra(LoginRequiredMixin, generic.ListView):
    template_name = "pinturas/list_era.html"
    model = Era
    context_object_name = "obj"
    login_url = "home:login"


class DetailEra(LoginRequiredMixin, generic.DetailView):
    template_name = "pinturas/detail_era.html"
    model = Era
    login_url = "home:login"
    context_object_name = "obj"


##### CREATE #####
## generic.CreateView
class NewEra(LoginRequiredMixin, generic.CreateView):
    template_name = "pinturas/new_era.html"
    model = Era
    context_object_name = "obj"
    form_class = NewEraForm
    login_url = "home:login"
    success_url = reverse_lazy("pinturas:list_era")

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super().form_valid(form)


##### UPDATE #####
## generic.UpdateView
class UpdateEra(LoginRequiredMixin, generic.UpdateView):
    template_name = "pinturas/update_era.html"
    model = Era
    context_object_name = "obj"
    form_class = NewEraForm
    login_url = "home:login"
    success_url = reverse_lazy("pinturas:list_era")


    def form_valid(self, form):
        form.instance.user = self.request.user
        return super().form_valid(form)

##### DELETE #####
## generic.DeleteView
class DeleteEra(LoginRequiredMixin, generic.DeleteView):
    template_name = "pinturas/delete_era.html"
    model = Era
    context_object_name = "obj"
    success_url = reverse_lazy("pinturas:list_era")
    login_url = "home:login"


##### API's #####
## ws JSON

def wsList(request):
    data = serializers.serialize('json', Category.objects.filter(status=False))
    return HttpResponse(data, content_type="application/json")

##### CRUD MUSEO #####

class ListMuseo(LoginRequiredMixin, generic.ListView):
    template_name = "pinturas/list_museo.html"
    model = Museo
    context_object_name = "obj"
    login_url = "home:login"


class NewMuseo(LoginRequiredMixin, generic.CreateView):
    template_name = "pinturas/new_museo.html"
    model = Museo
    form_class = MuseoForm
    login_url = "home:login"
    context_object_name = "obj"
    success_url = reverse_lazy("pinturas:list_museo")

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super().form_valid(form)


class UpdateMuseo(LoginRequiredMixin, generic.UpdateView):
    template_name = "pinturas/update_museo.html"
    model = Museo
    form_class = MuseoForm
    login_url = "home:login"
    context_object_name = "obj"
    success_url = reverse_lazy("pinturas:list_museo")

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super().form_valid(form)


class DeleteMuseo(LoginRequiredMixin, generic.DeleteView):
    template_name = "pinturas/delete_museo.html"
    model = Museo
    context_object_name = "obj"
    success_url = reverse_lazy("pinturas:list_museo")
    login_url = "home:login"

##### CRUD Artista #####


##### RETRIEVE (List, Detail) #####
## generic.ListView
class ListArtista(LoginRequiredMixin, generic.ListView):
    template_name = "pinturas/list_artista.html"
    model = Artista
    context_object_name = "obj"
    login_url = "home:login"


##### CREATE #####
## generic.CreateView
class NewArtista(LoginRequiredMixin, generic.CreateView):
    template_name = "pinturas/new_artista.html"
    model = Artista
    context_object_name = "obj"
    form_class = ArtistaForm
    login_url = "home:login"
    success_url = reverse_lazy("pinturas:list_artista")

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super().form_valid(form)

##### UPDATE #####
## generic.UpdateView
class UpdateArtista(LoginRequiredMixin, generic.UpdateView):
    template_name = "pinturas/update_artista.html"
    model = Artista
    context_object_name = "obj"
    form_class = ArtistaForm
    login_url = "home:login"
    success_url = reverse_lazy("pinturas:list_measure")

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super().form_valid(form)



##### DELETE #####
## generic.DeleteView
class DeleteArtista(LoginRequiredMixin, generic.DeleteView):
    template_name = "pinturas/delete_artista.html"
    model = Artista
    context_object_name = "obj"
    login_url = "home:login"
    success_url = reverse_lazy("pinturas:list_artista")


##### CRUD PRODUCT #####


##### RETRIEVE (List, Detail) #####
## generic.ListView
class ListObra(LoginRequiredMixin, generic.ListView):
    template_name = "pinturas/list_obra.html"
    model = Obra
    context_object_name = "obj"
    login_url = "home:login"


##### CREATE #####
## generic.CreateView
class NewObra(LoginRequiredMixin, generic.CreateView):
    template_name = "pinturas/new_obra2.html"
    model = Obra
    context_object_name = "obj"
    form_class = ObraForm
    login_url = "home:login"
    success_url = reverse_lazy("pinturas:list_obra")

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super().form_valid(form)

##### UPDATE #####
## generic.UpdateView
class UpdateObra(LoginRequiredMixin, generic.UpdateView):
    template_name = "pinturas/new_obra2.html"
    model = Obra
    context_object_name = "obj"
    form_class = ObraForm
    login_url = "home:login"
    success_url = reverse_lazy("pinturas:list_obra")

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super().form_valid(form)
